import { IsOptional } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import { _IsBooleanCustom } from '../../../shared/decorators/boolean.decorator';

export class UserLoginDto {

  @ApiProperty({
    description: 'Email',
    required: false,
    type: String
  })
  @IsOptional()
  readonly email: string;

  @ApiProperty({
    description: 'Password',
    required: false,
    type: String
  })
  @IsOptional()
  readonly password: string;
}

import { IsNotEmpty, IsBoolean , IsOptional } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class UserCreateDto {
  @ApiProperty({
    description: 'Name',
    required: true,
    type: String
  })
  @IsNotEmpty()
  readonly name: string;

  @ApiProperty({
    description: 'Email',
    required: true,
    type: String
  })
  @IsNotEmpty()
  readonly email: string;

  @ApiProperty({
    description: 'Password',
    required: true,
    type: String
  })
  @IsNotEmpty()
  readonly password: string;

  @ApiProperty({
    description: 'Status',
    required: true,
    type: Boolean
  })
  @IsNotEmpty()
  @IsBoolean()
  readonly status: boolean;

}

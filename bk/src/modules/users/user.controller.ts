import { Body, Controller, Get, Param, Patch, Post, Query, Delete } from '@nestjs/common';
import { ApiOkResponse, ApiTags, ApiCreatedResponse, ApiNoContentResponse } from '@nestjs/swagger';
import { UserService } from './user.service';
import { UserCreateDto } from './dtos/user-create.dto';
import { _SortObject } from '../../shared/decorators/sort-object.decorator';
import {
  SingleResponse,
  MultipleResponse
} from './swagger-models/user-response.module';
import { UserGetDto } from './dtos/user-get.dto';
import { UserLoginDto } from './dtos/user-login.dto';
import { InputTrimPipe } from '../../shared/pipes/input-trim.pipe';

@ApiTags('User')
@Controller('api/user')
export class UserController {
  constructor(private readonly service: UserService) { }

  @Get()
  @ApiOkResponse({ type: MultipleResponse })
  get(
    @Query(InputTrimPipe) userGetDto: UserGetDto,
    @_SortObject() sortObject: object
  ): Promise<object> {
    return this.service.get(userGetDto, sortObject);
  }

  @Post()
  @ApiCreatedResponse({ type: SingleResponse })
  createUser(
    @Body(InputTrimPipe) userCreateDto: UserCreateDto
  ): Promise<object> {
    return this.service.create(userCreateDto);
  }

  @Post('login')
  @ApiCreatedResponse({ type: SingleResponse })
  login(
    @Body(InputTrimPipe) userLoginDto: UserLoginDto
  ): Promise<object> {
    return this.service.login(userLoginDto);
  }

}
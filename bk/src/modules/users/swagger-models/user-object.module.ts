import { ApiProperty } from '@nestjs/swagger';

export class UserObject {
  @ApiProperty({
    description: 'Id of the User',
    type: String,
    uniqueItems: true,
  })
  id: string;

  @ApiProperty({
    description: 'Name',
    type: String,
  })
  name: string;

  @ApiProperty({
    description: 'Status',
    type: String,
  })
  status: string;

}

import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';

import { UserCreateDto } from './dtos/user-create.dto';
import { User } from '../../shared/entities/user.entity';
import { UserRepository } from '../../shared/repositories/user.repository';
import { BaseService } from '../../shared/services/base.service';
import { UserGetDto } from './dtos/user-get.dto';
import { UserLoginDto } from './dtos/user-login.dto';
import { _isEmpty, _toBoolean } from '../../shared/helpers/core/core-functions';
import { JwtService } from '@nestjs/jwt';


@Injectable()
export class UserService extends BaseService<User> {
  constructor(
    @InjectRepository(User)
    private readonly repository: UserRepository,
    private readonly jwtService: JwtService,

  ) {
    super(repository);
  }

  async create(userCreateDto: UserCreateDto): Promise<object> {
    return {
      data: { ...(await this.repository.createUser(userCreateDto)) },
      meta: {
        message: 'success'
      }
    };
  }
  async login(userLoginDto: UserLoginDto): Promise<object> {

    const data = await this.repository.login(userLoginDto);

    const jwt = await this.jwtService.signAsync({
      id: data['id'],
      name: data['name']
    })
    return {
      jwt: jwt,
      meta: {
        message: 'success'
      }
    };
  }


}
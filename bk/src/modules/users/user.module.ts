import { Module } from '@nestjs/common';
import { UserController } from './user.controller';
import { UserService } from './user.service';
import { UserRepository } from '../../shared/repositories/user.repository';
import { TypeOrmModule } from '@nestjs/typeorm';
import { jwtConstants } from '../../configs/jwt.config';
import { JwtModule } from '@nestjs/jwt';

@Module({
  imports: [
    TypeOrmModule.forFeature([UserRepository]),
    JwtModule.register({
      secret: jwtConstants.secret
    })
],
  controllers: [UserController],
  providers: [UserService],
})
export class UserModule {}

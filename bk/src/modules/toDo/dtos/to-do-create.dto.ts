import { IsNotEmpty, IsBoolean , IsOptional } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class ToDoCreateDto {
  @ApiProperty({
    description: 'Name',
    required: true,
    type: String
  })
  @IsNotEmpty()
  readonly name: string;


  @ApiProperty({
    description: 'Status',
    required: true,
    type: Boolean
  })
  @IsNotEmpty()
  @IsBoolean()
  readonly status: boolean;

}

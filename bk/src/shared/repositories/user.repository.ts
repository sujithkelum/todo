import { EntityRepository, Repository } from 'typeorm';
import { UserCreateDto } from '../../modules/users/dtos/user-create.dto';
import { UserLoginDto } from '../../modules/users/dtos/user-login.dto';
import { BadGatewayException, BadRequestException, InternalServerErrorException } from '@nestjs/common';
import { User } from '../entities/user.entity';
import { _isEmpty, _toBoolean } from '../helpers/core/core-functions';
import * as bcrypt from 'bcrypt';

@EntityRepository(User)
export class UserRepository extends Repository<User> {
  async createUser(userCreateDto: UserCreateDto): Promise<User> {
    try {
      const newUser = {
        ...this.create(),
        ...userCreateDto,
        created_at: new Date(),
        password: await bcrypt.hash(userCreateDto.password, 12)
      }

      return await this.save(newUser);;
    } catch (error) {
      throw new InternalServerErrorException({
      });
    }
  }

  async login(userLoginDto: UserLoginDto): Promise<object> {
     
    const user = await this.findOne({
      where: { email: userLoginDto.email }
    });

    if(!user){
      throw new BadRequestException("Invalid Credentials");
    }

    if(!await bcrypt.compare(userLoginDto.password,user.password)){
      throw new BadRequestException("Invalid Credentials");
    }
    return { ...user };
  }
}
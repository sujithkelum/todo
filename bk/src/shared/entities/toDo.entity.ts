import { Column, Entity, OneToMany } from 'typeorm';
import { MasterEntity } from './master.entity';

@Entity()
export class ToDo extends MasterEntity {
  @Column({ type: 'varchar', name: 'name' })
  name: string;

  @Column({ type: 'tinyint', default: false })
  status: boolean;

}

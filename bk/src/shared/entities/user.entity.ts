import { Column, Entity, OneToMany } from 'typeorm';
import { MasterEntity } from './master.entity';

@Entity()
export class User extends MasterEntity {
    @Column({ type: 'varchar', name: 'name' })
    name: string;

    @Column({ unique: true, type: 'varchar', name: 'email' })
    email: string;

    @Column({ type: 'varchar', name: 'password' })
    password: string;

    @Column({ type: 'tinyint', default: false })
    status: boolean;
}

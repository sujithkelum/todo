export const jwtConstants = {
  secret: process.env.JWT_SECRET
};

export const accessTokenExpiresIn = Number(process.env.ACCESS_KEY_EXPIRES_IN);



//core-methods
import {
    required, requiredIf, numeric,
    same, email, alphaNumeric, alphaSpecial, max, min,
    maxAmount,minAmount,requiredAtleastOne,requiredIFEmpty
} from "./core-methods";


//custom-methods    
// import {
// } from "./custom-methods";

export {
    //core-methods
    required,
    requiredIf,
    numeric,
    same,
    email,
    alphaNumeric,
    alphaSpecial,
    max,
    min,
    maxAmount,
    minAmount,
    requiredAtleastOne,
    requiredIFEmpty
    //custom-methods
}
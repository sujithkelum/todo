
import { apiBaseURL } from './core.config';

export const authAPI = {
  url: `${apiBaseURL}api/user/login`,
  key: 'getTokenKey',
};

export const toDoListAPI = {
  url: `${apiBaseURL}api/toDo`,
  key: 'toDoListAPIKey',
};

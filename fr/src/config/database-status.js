
const userStatus = {
  UNBLOCKED: true,
  BLOCKED: false,
};

const postStatus = {
  PENDING: 'PENDING',
  APPROVED: 'APPROVED',
  BLOCKED: 'BLOCKED',
};

const commentStatus = {
  UNBLOCKED: true,
  BLOCKED: false,
};

const roleCodes = {
  admin: 'ADMIN_USER',
  normalUser: 'NORMAL_USER',
};

export { userStatus, postStatus, commentStatus, roleCodes };

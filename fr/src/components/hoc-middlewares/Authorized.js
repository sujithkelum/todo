import React, { Fragment } from 'react';
import { Redirect } from 'react-router-dom';

import useInit from './useInit.hook';
import { usePermission } from '../hooks/common-hooks/usePermission.hook';
import { PermissionDenial } from '../modules/error-boundary/pages/PermissionDenial';

function authorized(Component) {
  function WrappedComponent(props) {
    const [initStatus, isAuth, authUser] = useInit(props.routeKey);

    const [, checkPermissionFn] = usePermission();

    return isAuth === null || initStatus === false ? null : (

      <Fragment>
        {' '}
        {isAuth === true ? (
          <Fragment>
            {checkPermissionFn(props.routePermissions) === true ? (
              <Component {...props} />
            ) : (
              <PermissionDenial />
            )}
          </Fragment>
        ) : (
          <Redirect to={'/login'} />
        )}{' '}
      </Fragment>

    );
  }

  return WrappedComponent;
}

export default authorized;

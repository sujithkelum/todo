
import { useRef,useEffect } from 'react';

const usePrevious=(props)=>{

    const ref = useRef();
  
    useEffect(() => {  
      ref.current = props;  
    }, [props]);

    return ref.current;
}

export {
    usePrevious
}
import React, { createContext, useReducer, useContext } from "react";

import { authAction } from "../core-helpers/authContext.helpers";
import { setAuthTokenKey, setUnauthorisedUserKey, setAuthUserObjectKey } from "../../../../config/actionKeys.config";

import { UIContext } from "../../../ui-components/context-providers/UIContext.provider"

const initialState = {
    accessToken: null,
    isAuthenticated: null,
    authUser: {
        "id": null,
        "name": null,
    }
};

const AuthContext = createContext({});

const authReducer = (state, action) => {

    switch (action.type) {
        case setAuthTokenKey:
            return {
                ...state,
                accessToken: action.playload,
                isAuthenticated: true
            };
        case setUnauthorisedUserKey:
            return {
                ...state,
                ...initialState,
                isAuthenticated: false
            }
        case setAuthUserObjectKey:
            return {
                ...state,
                authUser: {
                    ...state.authUser,
                    ...action.playload,
                }
            }
        default:
            return state;
    }
}

const AuthContextProvider = ({ children }) => {
    const [state, dispatch] = useReducer(authReducer, initialState);
    const [, uiDispatch] = useContext(UIContext);
    const dispatchFuntion = authAction(dispatch, uiDispatch);
    return (
        <AuthContext.Provider value={[state, dispatchFuntion]}>
            {children}
        </AuthContext.Provider>
    )
}

export {
    AuthContext,
    AuthContextProvider
}
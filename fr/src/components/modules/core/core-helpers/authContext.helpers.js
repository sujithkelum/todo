import {
  setAuthTokenKey,
  setUnauthorisedUserKey,
  setAuthUserObjectKey,
} from '../../../../config/actionKeys.config';
import { authAPI } from '../../../../config/apiUrl.config';
import {
  setAuthData,
  logoutUser,
} from '../../../../helpers/common-helpers/manageStorage.helpers';
import { callApi } from '../../../../helpers/common-helpers/callApi.helpers';
import { _get } from '../../../../helpers/common-helpers/lodash.wrappers';
import { responseCode } from '../../../../config/apiResponseKey';
/**
 * @description : set user auth token
 * @param {Function} dispatch auth  dispatch function
 * @param {Object} tokenObject
 */
const setTokensFn = (dispatch, tokenObject) => {
  dispatch({
    type: setAuthTokenKey,
    playload: tokenObject,
  });

  const userData = JSON.parse(atob(tokenObject.split('.')[1]));
  setAuthUserFn(dispatch, {
    id: userData.id,
    name: userData.name,
  });
};

/**
 * @description : unauthoried user
 * @param {Function} dispatch auth  dispatch function
 */
const unauthorisedUserFn = (dispatch) => {
  logoutUser();
  dispatch({
    type: setUnauthorisedUserKey,
  });
};

/**
 * @description : set user auth token
 * @param {Function} dispatch auth  dispatch function
 * @param {Object} userObject
 */
const setAuthUserFn = (dispatch, userObject) => {
  dispatch({
    type: setAuthUserObjectKey,
    playload: userObject,
  });
};

/**
 * @description : unauthoried user
 * @param {Function} dispatch auth  dispatch function
 * @param {Function} uiDispatch ui  dispatch function
 * @param {Object} formObject form object
 * @param {Object} formAction form dispatch function
 */
const onLoginFn = (
  dispatch,
  uiDispatch,
  formObject,
  formAction,
  formGroupKey
) => {
  uiDispatch.setPageLoader(true);
  callApi(authAPI.url)
    .body({
      email: formObject.email || '',
      password: formObject.password || '',
    })
    .headers(false)
    .method('post')
    .send((error, response) => {
      uiDispatch.setPageLoader(false);
      if (error) {
        if (
          _get(error, 'data.meta.code', undefined) ===
          responseCode.VALIDATION_ERROR
        ) {
         
          return formAction.setFormErrorFn(
            formGroupKey,
            _get(error, 'data.error', [])
          );
        } else if(_get(error, 'data.errors', undefined)) {
          return formAction.setFormErrorFn(formGroupKey, [
            {
              property: 'email',
              message: 'Invalid username or password',
            },
            {
              property: 'password',
              message: 'Invalid username or password',
            },
          ]);
        }
      } else {
        uiDispatch.setPageLoader(false);
        if (response.data.jwt) {
          setAuthData(response.data);
          setTokensFn(dispatch, response.data);
          return;
        } else {
          uiDispatch.setFlashMessage({
            status: true,
            message: 'Something went wrong please try again',
            messageType: 'error',
          });
          return;
        }
      }
    });
};

/**
 * @description connect all methods as one
 * @param {Object} dispatch
 */
const authAction = (dispatch, uiDispatch) => {
  return {
    setTokensFn: (tokenObject) => setTokensFn(dispatch, tokenObject),
    unauthorisedUserFn: () => unauthorisedUserFn(dispatch),
    onLoginFn: (formObject, formAction, formGroupKey) =>
      onLoginFn(dispatch, uiDispatch, formObject, formAction, formGroupKey),
  };
};

export { authAction };

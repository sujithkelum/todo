import React from 'react';

import { PageLoader } from '../../../../ui-components/templates/common-includes/PageLoader';
import { SnackBarList } from '../../../../ui-components/templates/common-includes/SnackbarWrapper';
import { Login } from './includes/Login';

const LoginRegister = () => {
  return (
    <div className="LoginMainWrapper">
      <SnackBarList />
      <Login />
      <PageLoader />
    </div>
  );
};

export default LoginRegister;

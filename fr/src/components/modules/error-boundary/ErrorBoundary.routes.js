
import React from 'react';

import { Navigate } from "../../ui-components/ui-elements/common/Navigate";
import PageError from "./pages/PageError";


const ErrorBoundaryRoutes = () => {
    return (
        <Navigate path="/page-error" exact={true} component={PageError} />
    )
};

export default ErrorBoundaryRoutes;

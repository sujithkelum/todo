import React, { Fragment, useEffect } from 'react';
import { useHistory } from 'react-router-dom';

import { TemplateOne } from '../../../ui-components/templates/TemplateOne';
import { ForumWrapper } from '../../../ui-components/ui-elements/forum/ForumWrapper';
import { toDoListAPI } from '../../../../config/apiUrl.config';
import { callApi } from '../../../../helpers/common-helpers/callApi.helpers';

const DashbordPage = () => {
  useEffect(() => {
    getInitialData();
}, []);


const data = null;

const getInitialData = () => {
    callApi(toDoListAPI.url)
        .method('get')
        .send((err, result) => {
            if (!err) {
                data = result.data.data;
            }
        })
}
  return (
    <TemplateOne>
     <ForumWrapper
          heading={'ToDo List'}
          apiUrl={`${toDoListAPI.url}`}
          forumKey={`${toDoListAPI.key}`}
          defaultSearchFormObject={{ serachkey: '' }}
        />
    </TemplateOne>
  );
};

export default DashbordPage;

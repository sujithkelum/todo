

import {
  setPageLoaderKey,
  setRouteKey,
  setFlashMessageKey,
  removeFlashMessageKey,
  setSideMenuStatusKey,
} from '../../../config/actionKeys.config';

import { dateObjectToString } from '../../../helpers/common-helpers/dateTime.helpers';

const setFlashMessage = (dispatch, messageObject) => {
  const key = dateObjectToString(new Date(), 'YYmmddHHMMII');

  dispatch({
    type: setFlashMessageKey,
    playload: {
      ...messageObject,
      key: key,
    },
  });

  setTimeout(() => {
    removeFlashMessage(dispatch, key);
  }, 5000);
};

const removeFlashMessage = (dispatch, key) => {
  dispatch({
    type: removeFlashMessageKey,
    key: key,
  });
};

const setSideToggleStatus = (dispatch, status) => {
  dispatch({
    type: setSideMenuStatusKey,
    playload: status,
  });
};

const setPageLoader = (dispatch, status) => {
  dispatch({
    type: setPageLoaderKey,
    playload: status,
  });
};

const setCurrentRouteFn = (dispatch, key) => {
  dispatch({
    type: setRouteKey,
    playload: key,
  });
};

const uiAction = (dispatch) => {
  return {
    setSideToggleStatus: (status) => setSideToggleStatus(dispatch, status),
    setPageLoader: (status) => setPageLoader(dispatch, status),
    setCurrentRouteFn: (key) => setCurrentRouteFn(dispatch, key),
    setFlashMessage: (messageObject) =>
      setFlashMessage(dispatch, messageObject),
    removeFlashMessage: (key) => removeFlashMessage(dispatch, key),
  };
};

export { uiAction };

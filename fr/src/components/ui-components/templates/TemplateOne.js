import React from 'react';
import { PageLoader } from './common-includes/PageLoader';
import { HeaderBar } from './common-includes/HeaderBar';

const TemplateOne = ({ children = null }) => {
  return (
    <div className={`templateOneWrapper bg-light`}>
      <HeaderBar />
      <div className="bodyWrapper container">{children}</div>
      <PageLoader />
    </div>
  );
};

export { TemplateOne };

import React, { useContext } from 'react';
import { Link } from 'react-router-dom';

import { AuthContext } from '../../../modules/core/context-providers/AuthContext.provider';

const HeaderBar = () => {
  const [authState] = useContext(AuthContext);
 
  return (
    <div className="navbar navbar-expand-lg fixed-top navbar-dark bg-dark">
      <div className="container-fluid">
        <span className="navbar-brand" href="#">
          ToDO
        </span>

        <div className="navbar-collapse offcanvas-collapse headerSubWrapper">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="nav-item">
              <Link className="nav-link active" to={'/home'}>
                Dashboard
              </Link>
            </li>

          </ul>
          <div className="profileInfo">
            <ul className="navbar-nav me-auto mb-2 mb-lg-0">
              <li className="nav-item ">
                <span className="nav-link">{authState.authUser.name}</span>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  );
};

export { HeaderBar };

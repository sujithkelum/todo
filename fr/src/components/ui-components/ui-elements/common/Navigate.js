
import React from 'react';
import {Route} from 'react-router-dom';
import {permissions} from "../../../../config/permission.config";

const Navigate=({
    component: Component,
    ...rest 
})=>{
    return <Route
                {...rest}
                render={(props) => <Component
                            {...props}
                            routeKey={rest.routeKey||""}
                            routePermissions={rest.routePermissions||permissions.NONE.permissions}
                        />}
            />
}


export {
    Navigate
}
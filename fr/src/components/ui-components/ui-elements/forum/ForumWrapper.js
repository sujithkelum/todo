import React, { Fragment, useState } from 'react';
import {
  emptyFun,
  UICard,
  VariableToComponent,
  CircleLoaderElement,
} from '../common/BaseElements';
import { InputButton, FormWrapper, InputButtonWithState, InputBoxWithState, SubmitButton, SelectBoxWithState } from '../form';
import { useForumState } from '../../../hooks/common-hooks/useForum.hook';
import { getDataByFormObject } from '../../../../helpers/common-helpers/common.helpers';

const From = ({
  forumKey = '',
  defaultSearchFormObject = {},
  setforumStateFn = emptyFun,
  forumState = {},
  requestAPIDataFn = emptyFun,
  apiUrl = ""
}) => {
  const apiUrlKey = forumKey;

  return (
    <UICard elementStyle="formWrapperCard defaultMarginBottom">
      <FormWrapper
        elementStyle="formWrapper"
        setGroupName={`${forumKey}_add`}
        formGroupLinkWith={forumKey}
        setFormObject={defaultSearchFormObject}
      >
        <div className="row fullWidthDiv">
          <VariableToComponent
            component={AddForm}
            props={{
              formKey: `${forumKey}_add`,
            }}
          />
        </div>

        <div className="fullWidthDiv buttonWrapper">
          <SubmitButton
            elementWrapperStyle={"floatRight"}
            btnText={"Add"}
            elementStyle="btnWrapper"
            startIcon={"far fa-save"}
            formGroupName={`${apiUrlKey}_add`}
            isValidate={false}
            flashMessages={{
              "success": {
                status: true,
                message: "Successfully added",
                messageType: "success"
              }
            }}

            callApiObject={{
              isSetHeaders: false,
              multipart: false,
              method: "post",
              onUpload: false
            }}
            apiDataStoringObject={{
              setLoader: true,
              storingType: "API_RESPONSE_LINKED_FORM",
              mergeToSuccessResponse: null,
              mergeToErrorResponse: null,
            }}
            onGetAPIEndPointFn={(formObject) => {
              return {
                url: apiUrl,
                key: forumKey
              }
            }}
            onChangeRequestBodyFn={(formObject) => {
              return getDataByFormObject(formObject);
            }}
            onResponseCallBackFn={(error) => {
              if (!error) {
                requestAPIDataFn({});
              }
            }}

          />
        </div>
      </FormWrapper>
    </UICard>
  );
};

const CardBodyWrapper = ({
  tableBody = [],
  forumKey = "",
  apiUrl = '',
  requestAPIDataFn = emptyFun,
}) => {
  return (
    <Fragment>

      {tableBody.map((item, index) => {
        return (
          <div key={index}>
            <UICard elementStyle={'detailWrapper'}>
              <Fragment>
                <div className="row">
                  <div className="col-md-4">{item.name}</div>
                  <div className="col-md-4">{item.status === 1 ? "Active" : "Inactive"}</div>
                  <div className="col-md-1">
                    <InputButton
                      btnText="Edit"
                    />
                  </div>
                  <div className="col-md-1">
                    <SubmitButton
                      elementWrapperStyle={"floatRight"}
                      btnText={"Remove"}
                      elementStyle="btnWrapper"
                      startIcon={"far fa-alt"}
                      formGroupName={forumKey}
                      isValidate={false}
                      flashMessages={{
                        "success": {
                          status: true,
                          message: "Successfully removed",
                          messageType: "success"
                        }
                      }}
                      callApiObject={{
                        isSetHeaders: false,
                        multipart: false,
                        method: "delete",
                        onUpload: false
                      }}
                      apiDataStoringObject={{
                        setLoader: true,
                        storingType: "API_RESPONSE_LINKED_FORM",
                        mergeToSuccessResponse: null,
                        mergeToErrorResponse: null,
                      }}
                      onGetAPIEndPointFn={(formObject) => {
                        return {
                          url: `${apiUrl}/${item.id}`,
                          key: forumKey
                        }
                      }}
                      onResponseCallBackFn={(error) => {
                        if (!error) {
                          requestAPIDataFn({});
                        }
                      }}
                    />
                  </div>
                </div>
              </Fragment>
            </UICard>
          </div>
        );
      })}
    </Fragment>
  );
};

const ForumWrapper = ({
  apiUrl = null,
  forumKey = '',
  isSetSearchFrom = true,
  
}) => {
  const [
    responseUpdateStatus,
    reloadStatus,
    responseFetchingStatus,
    pagingObject,
    tableBody,
    requestAPIDataFn,
    forumState,
    setforumStateFn,
  ] = useForumState(apiUrl, forumKey);
  return (
    <div className="forumWrapperStyle">
      <div className="align-items-center text-white rounded shadow-sm forumHeadingWrapper">
        <div className="col-md-1">
          <p>Name</p>
        </div>
        <div className="col-md-1">
          <p>Status</p>
        </div>
        <div className="filterIconWrapper">
          <h1 className="h6 mb-0 text-white lh-1">
            <span
              onClick={() =>
                setforumStateFn({
                  ...forumState,
                  isShowSearch: !forumState.isShowSearch,
                })
              }
            >
              <i className="mdi mdi-plus-circle" />
            </span>
          </h1>
        </div>
      </div>

      <Fragment>
        {forumState.isShowSearch === true && isSetSearchFrom === true ? (
          <From
            forumKey={forumKey}
            setforumStateFn={setforumStateFn}
            forumState={forumState}
            forumKey={forumKey}
            apiUrl={apiUrl}
            requestAPIDataFn={requestAPIDataFn}
          />
        ) : null}
      </Fragment>

      <Fragment>
        {apiUrl &&
          (responseFetchingStatus === 'init' ||
            responseFetchingStatus === undefined) ? (
          <div className="fullWidthDiv defaultMarginTopBottom">
            <center>
              <CircleLoaderElement />
            </center>
          </div>
        ) : (
          <Fragment>
            {responseFetchingStatus === 'error' || tableBody.length === 0 ? (
              <div className="fullWidthDiv defaultMarginTopBottom">
                No result found
              </div>
            ) : (
              <Fragment>
                <CardBodyWrapper
                  tableBody={tableBody}
                  forumKey={forumKey}
                  apiUrl={apiUrl}
                  requestAPIDataFn={requestAPIDataFn}
                />
              </Fragment>
            )}
          </Fragment>
        )}
      </Fragment>
    </div>
  );
};

const AddForm = (props) => {
  return (
    <div>
      <div className="col-md-6 floatLeft">
        <InputBoxWithState
          formGroupName={props.formKey}
          inputName={'name'}
          lableText={'Name'}
        />
      </div>
      <div className="col-md-6 floatLeft">

        <SelectBoxWithState
          formGroupName={props.formKey}
          dataList={[
            {
              id: "1",
              value: "Active",
            },
            {
              id: "0",
              value: "Inactive",
            },
          ]}
          inputName={"status"}
          emptySelectOptionTxt={"Select Status"}
          labelText="Status"
        />
      </div>
    </div>
  );
};

export { ForumWrapper };

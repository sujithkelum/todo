import React from 'react';

import {FormContextProvider} from "../components/modules/core/context-providers/FormContext.provider";
import {AuthContextProvider} from "../components/modules/core/context-providers/AuthContext.provider";
import {UIContextProvider} from "../components/ui-components/context-providers/UIContext.provider";
import {CoreContextProvider} from "../components/modules/core/context-providers/CoreContext.provider";


const ProviderComposer=({ contexts, children })=>{
    return contexts.reduceRight(
      (kids, parent) =>
        React.cloneElement(parent, {
          children: kids,
        }),
      children
    );
}

const ContextProvider=({ children })=>{
    return (
      <ProviderComposer
        contexts={[ 
          <UIContextProvider/>,
          <AuthContextProvider/>,
          <FormContextProvider/>,  
          <CoreContextProvider/>     
        ]}
      >
        {children}
      </ProviderComposer>
    );
  }
  
export { 
    ContextProvider 
};
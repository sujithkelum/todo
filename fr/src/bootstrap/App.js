import React from 'react';

import {ContextProvider} from "./Providers";
import {ThemeProvider} from "../components/ui-components/ui-elements/common/BaseElements";
import Routes from "./Routers";

const App=()=> {
  return (
    <ContextProvider>
    <ThemeProvider>
      <Routes/>
    </ThemeProvider>
    </ContextProvider>
  );
}

export default App;

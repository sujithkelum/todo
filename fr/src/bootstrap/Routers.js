import React, { Fragment } from 'react';
import { BrowserRouter, Switch } from 'react-router-dom';

//import routers
import AuthRoutes from '../components/modules/auth/Auth.routes';
import ErrorBoundary from '../components/ui-components/ui-elements/common/ErrorBoundary';
import DisplayError from '../components/modules/error-boundary/pages/DisplayError';
import ErrorBoundaryRoutes from '../components/modules/error-boundary/ErrorBoundary.routes';
import DashboardRoutes from '../components/modules/dashboard/Dashboard.routes';


const Routes = () => {
  return (
    <BrowserRouter>
      <Switch>
        <Fragment>
          <ErrorBoundary displayComponent={DisplayError}>
            <AuthRoutes />
            <DashboardRoutes />
            <ErrorBoundaryRoutes />
          </ErrorBoundary>
        </Fragment>
      </Switch>
    </BrowserRouter>
  );
};

export default Routes;
